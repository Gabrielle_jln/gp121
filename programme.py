# -- coding: utf-8 --
"""
Created on Mon Mar  7 13:50:14 2022

@author: Gabrielle JULIEN ; Eric FANG
"""

import tkinter as tk
from tkinter import ttk

import pandas as pd

import csv

programme = tk.Tk()
programme.title("FLIGHT ANALYSES SYSTEM")

Email1 = tk.StringVar()
Password1 = tk.StringVar()
Email2 = tk.StringVar()
Password2 = tk.StringVar()

F = open("user_info.csv", mode='r', encoding=None)
users = pd.read_csv(F, usecols=['number','email','password'])

print(users)
print(users['email'])


def login ():
    nom_utilisateur1 = str(Email1.get())
    for mot in users['email']:
        print(mot)
        print(nom_utilisateur1)
        if mot == nom_utilisateur1 :
            ttk.Label(fenetre, text="Welcome to the analyses system !" ).grid(column=3, row=5)
            break
        # reste à coder la partie qui modifie la date et l'heure de connexion
        # reste à vérifier que le mdp est le bon
        else:
            ttk.Label(fenetre, text="Not a valid user !  go to sign up " ).grid(column=3, row=5)
    


def signup ():
    nom_utilisateur2 = Email2.get()
    for mot in users['email']:
        print(mot)
        print(nom_utilisateur2)
        if mot == nom_utilisateur2 :
            ttk.Label(fenetre, text="Already signed up email, go to Login" ).grid(column=3, row=9)
            break
        else:
            ttk.Label(fenetre, text="New user is registered" ).grid(column=3, row=9)
            # reste à modifier le user_info
    return



fenetre = ttk.Frame(programme, padding="1i 1i 0.5i 0.5i")
fenetre.grid(column=0, row=0) 

tk.Label(fenetre, text="WELCOME TO FLIGHT ANALYSES SYSTEM" ).grid(column=3, row=1)
							  
Entree1 = ttk.Entry(fenetre, width=15, textvariable = Email1)
Texte1 = tk.Label(fenetre, text="Email : ", bg="blue", fg="white")
Entree1.grid(column= 3, row=2)
Texte1.grid(column= 2, row=2)
Entree2 = ttk.Entry(fenetre, width=15, textvariable = Password1)
Texte2 = tk.Label(fenetre, text="Password : ", bg='blue', fg='white')
Entree2.grid(column= 3, row=3)
Texte2.grid(column= 2, row=3)
ttk.Button(fenetre, text="(Login)", command=login).grid(column=3, row=4)
programme.bind('<Return>', login) 

Entree3 = ttk.Entry(fenetre, width=15, textvariable = Email2)
Texte3 = tk.Label(fenetre, text="Email : ", bg='orange', fg='white')
Entree3.grid(column= 3, row=6)
Texte3.grid(column= 2, row=6)
Entree4 = ttk.Entry(fenetre, width=15, textvariable = Password2)
Texte4 = tk.Label(fenetre, text="Password : ", bg='orange', fg='white')
Entree4.grid(column= 3, row=7)
Texte4.grid(column= 2, row=7)
ttk.Button(fenetre, text="(Sign Up)", command=signup).grid(column=3, row=8)
programme.bind('<Return>', login) 




programme.mainloop()
F.close()